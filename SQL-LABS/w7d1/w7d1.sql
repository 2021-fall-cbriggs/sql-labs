DROP TABLE IF EXISTS countries;
DROP TABLE IF EXISTS jobs;
DROP TABLE IF EXISTS job_history;
DROP TABLE IF EXISTS employees;
DROP TABLE IF EXISTS departments;
DROP TABLE IF EXISTS car;
DROP TABLE IF EXISTS lot;
DROP TABLE IF EXISTS employee;


-- 1
CREATE TABLE countries(
	country_id INTEGER,
	country_name VARCHAR(32),
	region_id INTEGER
);

-- 2
CREATE TABLE jobs(
	job_id INTEGER,
	job_title VARCHAR(32),
	min_salary INTEGER,
	max_salary INTEGER
);

-- 3
CREATE TABLE job_history(
	employee_id INTEGER,
	start_date TIMESTAMP,
	end_date TIMESTAMP,
	job_id INTEGER,
	department_id INTEGER
);

-- 4
CREATE TABLE departments(
	department_id DECIMAL(4,0) DEFAULT 0 UNIQUE NOT NULL,
	department_name VARCHAR(30) UNIQUE NOT NULL,
	manager_id DECIMAL(6,0) DEFAULT 0 UNIQUE NOT NULL,
	locaton_id DECIMAL(4,0) DEFAULT NULL,
	PRIMARY KEY(department_id, manager_id)
);

-- 5 
CREATE TABLE employees(
	employee_id INTEGER UNIQUE,
	first_name VARCHAR(16),
	last_name VARCHAR(16),
	email VARCHAR(32),
	phone_number VARCHAR(16),
	hire_date TIMESTAMP,
	job_id INTEGER,
	salary INTEGER,
	commission DECIMAL(10,2),
	manager_id DECIMAL(6,0) DEFAULT 0 UNIQUE NOT NULL,
	department_id DECIMAL(4,0) DEFAULT 0 UNIQUE NOT NULL,
	PRIMARY KEY(manager_id, department_id),
	CONSTRAINT department_id_manager_id_fk FOREIGN KEY (department_id, manager_id) REFERENCES departments(department_id, manager_id)
);

-- 6

CREATE TABLE employee(
	employee_id INTEGER AUTO_INCREMENT,
	name VARCHAR(32),
	start_date TIMESTAMP,
	term_date TIMESTAMP,
	username VARCHAR(16),
	birthdate TIMESTAMP,
	status VARCHAR(16),
	PRIMARY KEY(employee_id)
);

CREATE TABLE lot(
	lot_number INTEGER AUTO_INCREMENT,
	address VARCHAR(128),
	employee_id INTEGER,
	PRIMARY KEY(lot_number),
	CONSTRAINT employee_id_fk FOREIGN KEY (employee_id) REFERENCES employee(employee_id)
);

CREATE TABLE car(
	car_id INTEGER AUTO_INCREMENT,
	make VARCHAR(16),
	model VARCHAR(16),
	year INTEGER,
	mileage INTEGER,
	image VARCHAR(128),
	lot_number INTEGER,
	PRIMARY KEY(car_id),
	CONSTRAINT lot_number_fk FOREIGN KEY (lot_number) REFERENCES lot(lot_number)
);