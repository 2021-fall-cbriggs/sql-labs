-- Insert into employee
INSERT INTO employee(name, start_date, status)
VALUES ('Frank', '1992-07-25', 'Active');
INSERT INTO employee(name, start_date, status)
VALUES ('Susan', '2002-05-01', 'Active');
INSERT INTO employee(name, start_date, term_date, status)
VALUES ('John', '1998-02-12', '2005-08-23', 'Inactive');
INSERT INTO employee(name, start_date, status)
VALUES ('Allen', '2010-10-31', 'Active');

-- Insert into lot
INSERT INTO lot(address, employee_id) 
VALUES ('123 Fake Street',
			(
				SELECT employee_id
			  	  FROM employee
		   	 	 WHERE name = 'Frank'
			)
	    ),
	    ('394 Fun Street',
	    	(
	    		SELECT employee_id
			  	  FROM employee
		   	 	 WHERE name = 'Susan'
	    	)
	    );

-- Insert into car
INSERT INTO car(make, model, year, mileage, lot_number)
VALUES ('Toyota', 'Corolla', 2008, 100000,
			(
				SELECT lot_number
			  	  FROM lot
		   	 	 WHERE address = '123 Fake Street'
			)
		),
		('Mazda', '3', 2015, 45000,
			(
				SELECT lot_number
			  	  FROM lot
		   	 	 WHERE address = '123 Fake Street'
			)
		),
		('Ford', 'Taurus', 2010, 124000,
			(
				SELECT lot_number
			  	  FROM lot
		   	 	 WHERE address = '123 Fake Street'
			)
		),
		('Chrysler', 'PT Cruiser', 2008, 88000,
			(
				SELECT lot_number
			  	  FROM lot
		   	 	 WHERE address = '394 Fun Street'
			)
		),
		('BMW', 'M3', 2011, 95000,
			(
				SELECT lot_number
			  	  FROM lot
		   	 	 WHERE address = '394 Fun Street'
			)
		),
		('Toyota', 'Camry', 1999, 200000,
			(
				SELECT lot_number
			  	  FROM lot
		   	 	 WHERE address = '394 Fun Street'
			)
		);

-- Selects
SELECT *
FROM employee
WHERE status = 'Active';

SELECT *
FROM employee
WHERE start_date < '2000-01-01';

SELECT *
FROM car
WHERE make = 'Toyota' AND
	  mileage < 200000;
	 
SELECT * 
FROM car
WHERE year > 2005 AND 
	  mileage > 80000;